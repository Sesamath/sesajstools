# sesajstools

Le dépôt de code a déménagé et se trouve désormais sur
https://git.sesamath.net/sesamath/sesajstools

Vous pouvez indiquez dans vos dépendances (package.json)
```
  "sesajstools": "git+https://git.sesamath.net/sesamath/sesajstools.git",
```
